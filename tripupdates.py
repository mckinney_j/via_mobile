from google.transit import gtfs_realtime_pb2
import requests

f= open("tripupdates.txt","w+")
feed = gtfs_realtime_pb2.FeedMessage()
response = requests.get('http://gtfs.viainfo.net/tripupdate/tripupdates.pb')
feed.ParseFromString(response.content)

f.write(str(feed))
f.close() 