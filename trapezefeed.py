from google.transit import gtfs_realtime_pb2
import requests

f= open("testdata.txt","w+")
feed = gtfs_realtime_pb2.FeedMessage()
response = requests.get('http://gtfs.viainfo.net/gtfs-realtime/trapezerealtimefeed.pb')
feed.ParseFromString(response.content)

for entity in feed.entity:
  if entity.id=="210":
      f.write(str(entity))
f.close() 